// Try HACK TO THE FUTURE 2018 qual
// author: Leonardone @ NEETSDKASU

macro_rules! ewriteln {
  ($fmt:expr)  => {{
    use std::io::Write;
    writeln!(&mut std::io::stderr(), $fmt).unwrap();
  }};
  ($fmt:expr, $($arg:tt)*) => {{
    use std::io::Write;
    writeln!(&mut std::io::stderr(), $fmt, $($arg)*).unwrap();
  }};
}

type FieldType = Vec<Vec<i64>>;
type ResultType = Vec<(usize, usize, usize)>;

fn main() {
    use std::time::{Duration, SystemTime};
    let loop_out_time = SystemTime::now() + Duration::from_millis(5900);

    let mut stdin = String::new();
    std::io::Read::read_to_string(
        &mut std::io::stdin(),
        &mut stdin).unwrap();
    let mut stdin = stdin.split_whitespace();
    let mut get = || stdin.next().unwrap().parse().unwrap();

    let mut field = vec![vec![0_i64; 100]; 100];

    for y in 0..100 {
        for x in 0..100 {
            field[y][x] = get();
        }
    }

    let mut xsft = XorShift::new();

    let mut ans = vec![(0, 0, 0); 1000];
    let mut max_score = 0_i64;
    
    let mut lp = 0_u64;
    
    loop {
    
        lp += 1;

        let mut table = vec![vec![0_i64; 100]; 100];
        let mut tmp = vec![(0, 0, 0); 1000];

        gen(&mut xsft, &mut table, &mut tmp);

        let score = calc_score(&field, &table);

        if score > max_score {
            max_score = score;
            ans = tmp;
        }

        let now = SystemTime::now();
        if now.cmp(&loop_out_time) == std::cmp::Ordering::Greater {
            break;
        }

    }

    ewriteln!("Loops = {}", lp);
    ewriteln!("Score = {}", max_score);

    println!("{}", ans.len());
    for &(x, y, h) in ans.iter() {
        println!("{} {} {}", x, y, h);
    }
}

fn calc_score(field: &FieldType, table: &FieldType) -> i64 {
    let mut score = 200_000_000_i64;
    for y in 0..100 {
        for x in 0..100 {
            score -= (table[y][x] - field[y][x]).abs();
        }
    }
    score
}

fn gen(xsft: &mut XorShift, table: &mut FieldType, tmp: &mut ResultType) {
    for i in 0..1000 {
        let x = xsft.next_usize(100);
        let y = xsft.next_usize(100);
        let h = xsft.next_usize(100) + 1;
        tmp[i] = (x, y, h);
        let x0 = if x < h { 0 } else { x - h };
        let y0 = if y < h { 0 } else { y - h };
        let x1 = std::cmp::min(99, x + h);
        let y1 = std::cmp::min(99, y + h);
        for yi in y0..y1+1 {
            for xi in x0..x1+1 {
                let px = (xi as i64 - x as i64).abs();
                let py = (yi as i64 - y as i64).abs();
                let ph = h as i64 - px - py;
                if ph < 0 { continue; }
                table[yi][xi] += ph;
            }
        }
    }
}

struct XorShift {
    value: u64
}

impl XorShift {
    fn new() -> XorShift {
        XorShift{ value: 88172645463325252 }
    }
    fn next(&mut self) -> u64 {
        self.value = self.value ^ (self.value << 13);
        self.value = self.value ^ (self.value >> 17);
        self.value = self.value ^ (self.value << 5);
        self.value
    }
    fn next_usize(&mut self, x: usize) -> usize {
        self.next() as usize % x
    }
}
